rgarrigue.common
================

My common stuff for linux servers or workstations

- disable IPv6
- install entropy daemon
- install fail2ban
- tweak openssh
- install cli tools like nmap, glances, unzip, tar, ...
- set swappiness to 10
- install open-vm-tools if running over vmware

Will be improved now and then with *my* good practises

Cosmetic *personnal* stuff like `vimrc` goes in `rgarrigue.cosmetic`

Requirements
------------

Developped and tested on *Ubuntu Server 16.10 Yakkety*, and *CentOS 6 & 7*

Role Variables
--------------

List of local ssh keys to install in the server

- `common_ssh_keys` default to nothing, see commented example in defaults/main.yml

Dependencies
------------

None

Example Playbook
----------------

    - hosts: all
      roles:
        - role: rgarrigue.common

License
-------

GPLv3

Author Information
------------------

Rémy Garrigue

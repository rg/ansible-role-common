import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.mark.parametrize("path", [("/var/log/auth.log")])
def test_files_exists(host, path):
    f = host.file(path)
    assert f.exists


@pytest.mark.parametrize("path", [("/root/anaconda-ks.cfg")])
def test_files_do_not_exist(host, path):
    f = host.file(path)
    assert not f.exists


@pytest.mark.parametrize(
    "package",
    [
        "bash-completion",
        "curl",
        "dos2unix",
        "etckeeper",
        "fail2ban",
        "git",
        "glances",
        "htop",
        "iftop",
        "iotop",
        "jq",
        "lsof",
        "mlocate",
        "net-tools",
        "nmap",
        "ntp",
        "psmisc",
        "rng-tools",
        "rsync",
        "screen",
        "sysstat",
        "tar",
        "tcpdump",
        "tree",
        "unzip",
        "vim",
        "wget",
        "zip",
    ],
)
def test_package_installed(host, package):
    p = host.package(package)
    assert p.is_installed
